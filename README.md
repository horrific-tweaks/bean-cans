This datapack recreates the functionality from the [Inconspicuous Bean Can](https://www.curseforge.com/minecraft/mc-mods/inconspicuous-bean-can) forge mod as a datapack+resourcepack. It uses a named rotten flesh item with a CustomModelData property as a "can" & tracks its use with a scoreboard counter.

There is also a modified loot table for zombies & husks, as well as a custom advancement that activates upon obtaining a bean can.
