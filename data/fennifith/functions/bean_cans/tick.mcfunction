# Tag all players holding the item + nbtdata
tag @a[nbt={SelectedItem:{id:"minecraft:<< items.bean_can.id >>",tag:{CustomModelData:<< items.bean_can.nbt.CustomModelData >>}}},tag=!beanCan] add beanCan
# Execute as players that were holding the item, only if they have used it
execute as @a[tag=beanCan] if score @s bean_cans.used matches 1.. run function fennifith:bean_cans/private/used
# Remove tag from players that are no longer holding n item
tag @a[nbt=!{SelectedItem:{id:"minecraft:<< items.bean_can.id >>",tag:{CustomModelData:<< items.bean_can.nbt.CustomModelData >>}}},tag=beanCan] remove beanCan
# Reset the used potion scoreboard
scoreboard players set @a bean_cans.used 0
