# Removes items from all armor slots
# use '/data get entity @s Inventory' to test for changes to the slot index

# format: [index]b [motion] [slot]
<% set positions %>
100b 0.3,0.3,0.3 armor.head
101b 0.2,0.3,-0.3 armor.chest
102b -0.4,0.3,0.1 armor.legs
103b -0.2,0.3,-0.2 armor.feet
<% endset %>

<% set positions = positions | trim %>

<% for position in positions.split("\n") %>
<% set parts = position.split(" ") %>
execute if data entity @s Inventory[{Slot:<< parts[0] >>}] run summon item ~ ~1 ~ {Item:{id:"minecraft:stone",Count:1b},Tags:["dropped"],Motion:[<< parts[1] >>],PickupDelay:20}
data modify entity @e[type=item,tag=dropped,limit=1] Item set from entity @s Inventory[{Slot:<< parts[0] >>}]
tag @e[type=item] remove dropped
<% endfor %>

<% for position in positions.split("\n") %>
<% set parts = position.split(" ") %>
item replace entity @s << parts[2] >> with minecraft:air

<% set motion = parts[1].split(",") %>
playsound item.armor.equip_generic player @a ~<< motion[0] >> ~<< motion[1] >> ~<< motion[2] >>
<% endfor %>
